<?php


/**
 * Display/validate/process a form to generate a peek session
 */
function peek_generate_form(&$form_state, $node) {
  $form['peek_generate'] = array(
    '#type' => 'fieldset',
    '#title' => t('New peek'),
    '#description' => t('Enter the email addresses that the Peek should be sent to. Separate multiple email addresses with commas.'),
  );
  $form['peek_generate']['node'] = array(
    '#type' => 'value',
    '#value' => $node,
  );
  $form['peek_generate']['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#required' => TRUE,
    '#description' => t('These email addresses will receive an an email with the Peek link. If the email address is not associated with a user account on this site a new account will be created for them.'),
  );
  $form['peek_generate']['bcc'] = array(
    '#type' => 'textfield',
    '#title' => t('Bcc'),
    '#description' => t('These email addresses will receive an BCCed copied of the email sent to the user.'),
  );
  $form['peek_generate']['notify'] = array(
    '#type' => 'textfield',
    '#title' => t('Notify'),
    '#description' => t('These email addresses will receive an email the first time the Peek is accessed.'),
  );
  $form['peek_generate']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#cols' => 20,
    '#description' => t("An optional message to help the recipient understand why they've been granted access to this content."),
  );
  $form['peek_generate']['submit'] = array('#type' => 'submit', '#value' => t('Generate a peek'), '#weight' => 20);
  return $form;
}

function peek_generate_form_validate($form, &$form_state) {
  // validate the email formats in each of the email fields
  foreach (array('mail', 'bcc', 'notify') as $key) {
    if ($value = $form_state['values'][$key]) {
      foreach (explode(', ' , $value) as $mail) {
        if (!valid_email_address($mail)) {
          form_set_error($key, t('The e-mail address %mail is not valid.', array('%mail' => $mail)));
        }
      }
    }
  }
}

function peek_generate_form_submit($form, &$form_state) {
  global $user;

  $node = $form_state['values']['node'];
  $to = explode(', ', $form_state['values']['mail']);;
  $from = $user->mail ? $user->mail : variable_get('site_mail', ini_get('sendmail_from'));

  foreach ($to as $mail) {
    $peek = peek_fetch($node, $mail, $form_state['values']['notify'], $form_state['values']['message']);
    if ($peek) {
      $account = user_load(array('mail' => $mail));
      $params = array(
        'node' => $node,
        'peek' => $peek,
        'recipient' => $account,
        'creator' => $user,
        'bcc' => $form_state['values']['bcc'],
      );
      drupal_mail('peek', 'created', $mail, user_preferred_language($account), $params, empty($user->mail) ? NULL : $user->mail);

      drupal_set_message(t('Sent a peek to %to', array('%to' => $mail)));
      watchdog('peek log', 'Peek sent to %to', array('%to' => $mail), WATCHDOG_NOTICE);
    }
  }

  $form_state['redirect'] = array('node/'. $node->nid .'/peek');
}


/**
 * Display a page with the user's peeks.
 *
 * @param $user object
 */
function peek_user_page($user) {
  $output = peek_table(array('uid' => $user->uid));
  if (!$output) {
    $output = t('No peeks');
  }
  return $output;
}

/**
 * Display a page with the node's peeks.
 *
 * @param $node
 */
function peek_node_page($node) {
  $output = '';

  if ($node->nodepeeks) {
    $table = peek_table(array('nid' => $node->nid));
    $output .= empty($table) ? t('No peeks') : $table;
  }
  if ($node->peekable) {
    $output .= drupal_get_form('peek_generate_form', $node);
  }

  return $output;
}

/**
 * peek_show
 *
 * Show a peek of a node
 *
 */
function peek_show($user, $peek) {
  $current = time();
  $node = node_load($peek->nid);

  if (!$peek->first_access) {
    $peek->first_access = $current;
    drupal_write_record('peek', $peek, 'sid');

    watchdog('peek log', 'First peek for sid: %s', array('%s' => $peek->sid), WATCHDOG_WARNING);
    if (!empty($peek->notify)) {
      $params = array(
        'node' => $node,
        'peek' => $peek,
        'recipient' => $user,
      );
      foreach (explode(', ', $peek->notify) as $mail) {
        drupal_mail('peek', 'accessed', $mail, language_default(), $params);
        watchdog('peek log', 'Notified %to that %title was accessed', array('%to' => $mail, '%title' => $node->title), WATCHDOG_NOTICE);
      }
    }
  }

  // Record that the peek was accessed.
  $peek_access = array(
    'sid' => $peek->sid,
    'hostname' => ip_address(),
    'timestamp' => $current,
  );
  drupal_write_record('peek_access', $peek_access);

  $path = 'node/'. $peek->nid;

  // If the user has permissions to view the node just redirect them.
  // switch to the node page , or otherwise just pretend i'm actually on the node page */
  if (!empty($user->status) && node_access('view', $node)) {
    drupal_goto($path);
  }

  menu_set_active_item($path);
  if ($node = node_load($peek->nid)) {
    // Let other modules know that we're peeking so they can set global
    // variables to indicate that permission checks should be bypassed.
    module_invoke_all('peek_peeking', $peek, $node);

    return node_page_view($node);
  }
}
