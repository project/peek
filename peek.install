<?php

/**
 * Implementation of hook_install().
 */
function peek_schema() {
  $schema['peek'] = array(
    'description' => t('Table tracking the peekable permissions for nodes.'),
    'fields' => array(
      'sid' => array(
        'description' => t('Hash id created from the created timestamp, node and user id.'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'nid' => array(
        'description' => t('Node id.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => t("User id that's allowed to view this."),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => t('The Unix timestamp when the access was created.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'expires' => array(
        'description' => t('The Unix timestamp when the access expires.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'duration' => array(
        'description' => t('Length in seconds that the peek will be valid once it is first accessed.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'first_access' => array(
        'description' => t('The Unix timestamp when the peek was first used.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'notify' => array(
        'description' => t('Comma separated list of email addresses to notify when the peek is first accessed.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'message' => array(
        'description' => t('An optional message describing the page being peeked.'),
        'type' => 'text',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array(
      'peek_nid'        => array('nid'),
      'peek_uid'        => array('uid'),
      'peek_created'    => array('created'),
    ),
  );

  $schema['peek_access'] = array(
    'fields' => array(
      'access_id' => array(
        'description' => t('Peek access id.'),
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => t('The id of the peek being accessed.'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => ''
      ),
      'hostname' => array(
        'description' => t('IP address that accessed the peek.'),
        'type' => 'varchar',
        'length' => 128,
      ),
      'timestamp' => array(
        'description' => t('The Unix timestamp when the access occurred.'),
        'type' => 'int',
      ),
    ),
    'primary key' => array('access_id'),
    'indexes' => array(
      'peek_access_sid_timestamp' => array('sid', 'timestamp'),
    ),
  );

  $schema['peekable'] = array(
    'fields' => array(
      'nid' => array(
        'description' => t('Is this node peekable.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('nid'),
  );

  return $schema;
}

/**
 * Implementation of hook_install().
 */
function peek_install() {
  drupal_install_schema('peek');
}

/**
 * Implementation of hook_uninstall().
 */
function peek_uninstall() {
  drupal_uninstall_schema('peek');
}

function peek_update_1() {
  $ret = array();
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      $ret[] = update_sql("CREATE TABLE IF NOT EXISTS {peekable} (
        nid int(10) unsigned NOT NULL default '0',
        PRIMARY KEY (nid)
      );");
      break;
    case 'pgsql':
      die('TODO: pgsql');
      break;
  }
  drupal_set_message("peekability database table created.");
  return $ret;
}

function peek_update_2() {
  $ret = array();
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      $ret[] = update_sql("ALTER TABLE {peek} ADD notify varchar(255) NOT NULL default ''");
      break;
    case 'pgsql':
      die('TODO: pgsql');
      break;
  }
  drupal_set_message("peek notification field added.");
  return $ret;
}

/**
 * Rename the poorly named {peek_access}.peek_id field to {peek_access}.acces_id.
 */
function peek_update_6000() {
  $field = array(
    'description' => t('Id to uniquely identify an access record.'),
    'type' => 'serial',
    'not null' => TRUE,
  );
  db_change_field($ret, 'peek_access', 'peek_id', 'access_id', $field);
  return $ret;
}

/**
 * Add a message field to the peek.
 */
function peek_update_6001() {
  $ret = array();
  $field = array(
    'description' => t('An optional message describing the page being peeked.'),
    'type' => 'text',
    'not null' => FALSE,
  );
  db_add_field($ret, 'peek', 'message', $field);
  return $ret;
}