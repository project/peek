<?php

/**
 * Menu callback for the peek settings form.
 */
function peek_admin_settings() {
  $duration = (integer) variable_get('peek_duration', 3600);
  $expiry = (integer) variable_get('peek_expiry', 3 * 24 * 3600);
  $form['settings_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default settings'),
  );
  $form['settings_general']['peek_anonymous'] = array(
    '#type' => 'checkbox',
    '#title' => t('Anonymous Peeks'),
    '#return_value' => 1,
    '#default_value' => variable_get('peek_anonymous', 0),
    '#description' => t("If checked, don't generate new users when generating a peek for a non-user, use anonymous access."),
  );
  $form['settings_general']['peek_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Peek duration'),
    '#default_value' => $duration / 3600,
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('Length of time in hours that the peek link remains valid after first access.'),
    '#field_suffix' => t('hours')
  );
  $form['settings_general']['peek_expiry'] = array(
    '#type' => 'textfield',
    '#title' => t('Peek expiry'),
    '#default_value' => $expiry / (24 * 3600),
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('Length of time in days that the first access of the peek link remains valid.'),
    '#field_suffix' => t('days'),
  );
  // Inject our won submit handler to convert the units.
  $form['#submit'][] = 'peek_admin_settings_submit';
  $form['#validate'][] = 'peek_admin_settings_validate';
  return system_settings_form($form);
}

/**
 * Form API callbacks to validate and submit the peek settings form.
 */
function peek_admin_settings_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['peek_duration'])) {
    form_set_error('peek_duration', t('You must set the peek duration to a number.'));
  }
  if (!is_numeric($form_state['values']['peek_expiry'])) {
    form_set_error('peek_expiry', t('You must set the peek expiry to a number.'));
  }
}

function peek_admin_settings_submit($form, &$form_state) {
  // Convert the format and the let system_settings_form_submit() save them.
  $form_state['values']['peek_duration'] = (integer) (3600 * $form_state['values']['peek_duration']);
  $form_state['values']['peek_expiry'] = (integer) (24 * 3600 * $form_state['values']['peek_expiry']);
}

/**
 * peek_peek
 *
 * Display full detail about a peek access times and provide an opportunity to delete it.
 *
 * @param $peek A peek object returned by peek_load().
 */
function peek_peek_form(&$form_state, $peek) {
  $form['peek_info']['sid'] = array('#type' => 'value', '#name' => 'peek', '#value' => $peek->sid);

  $status = implode(', ', _peek_status($peek));
  $status = $status ? t('Inactive: %status', array('%status' => $status)) : t('Active');
  $rows = array(
    array(t('Title'), l($peek->title, 'node/'. $peek->nid)),
    array(t('User'), l($peek->mail, 'user/'. $peek->uid)),
    array(t('Created'), format_date($peek->created)),
    array(t('Expires'), format_date($peek->expires)),
    array(t('Duration'), format_interval($peek->duration)),
    array(t('First Access'), empty($peek->first_access) ? theme('placeholder', t('None')) : format_date($peek->first_access)),
    array(t('Notification'), empty($peek->notify) ? theme('placeholder', t('None')) : check_plain($peek->notify)),
    array(t('Message'), empty($peek->message) ? theme('placeholder', t('None')) : check_plain($peek->message)),
    array(t('Status'), $status),
  );

  $form['peek_info']['peek'] = array('#type' => 'markup', '#value' => theme('table', array(), $rows));
  $form['peek_info']['delete'] = array('#type' => 'submit', '#value' => t('Delete'));

  $result = db_query('SELECT hostname, timestamp FROM {peek_access} WHERE sid = "%s"', $peek->sid);
  $rows = array();
  while ($p = db_fetch_array($result)) {
    $p['timestamp'] = format_date($p['timestamp']);
    $rows[] = $p;
  }
  if ($rows) {
    $header = array('IP', 'Date');
    $log = theme('table', $header, $rows);
    $form['peek_log'] = array(
      '#type' => 'fieldset',
      '#title' => t('Access log'),
      '#description' => t('This peek was accessed from the following IP addresses at the times indicated.')
    );
    $form['peek_log']['log'] = array('#type' => 'markup', '#value' => $log);
  }
  return $form;
}

function peek_peek_form_submit($form, &$form_state) {
  $sid = $form_state['values']['sid'];

  db_query("DELETE FROM {peek} WHERE sid = '%s'", $sid);
  db_query("DELETE FROM {peek_access} WHERE sid = '%s'", $sid);

  drupal_set_message(t('Peek deleted.'));
  $form_state['redirect'] = 'admin/content/peek';
}

/**
 * peek_content_administer
 *
 * Display a list of generated peeks and their status
 *
 */
function peek_content_administer() {
  $output = peek_table(array());
  if (!$output) {
    $output = t('No peeks');
  }
  return $output;
}

function peek_content_administer_unpeeked() {
  $output = peek_table(array('first_access' => 0));
  if (!$output) {
    $output = t('No peeks');
  }
  return $output;
}